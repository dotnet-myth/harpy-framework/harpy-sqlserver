﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Data;

namespace Harpy.SQLServer.IoC {

    public static class SqlServerContainer {

        public static IDbConnection UseSQLServerConnection( this DbContextOptionsBuilder dbContextOptions, string connectionString = null, Action<SqlServerDbContextOptionsBuilder> sqlServerOptionsAction = null ) {
            if ( string.IsNullOrEmpty( connectionString ) )
                connectionString = "Data Source=:memory:";

            var dbConnection = new SqlConnection( connectionString );
            dbConnection.Open( );

            dbContextOptions.UseSqlServer( dbConnection, sqlServerOptionsAction );

            return dbConnection;
        }

        public static SqlServerDbContextOptionsBuilder AddMigrations( this SqlServerDbContextOptionsBuilder optionsBuilder, string migrationsAssembly = "" ) {
            return optionsBuilder
                .MigrationsAssembly( migrationsAssembly )
                .MigrationsHistoryTable( "migrations_history" );
        }
    }
}