﻿using Hangfire.SqlServer;
using System;

namespace Harpy.SQLServer.Presentation.Extensions {

    public static class HangfireSQLServer {

        public static SqlServerStorage UseStorage( string hangfireConnection = null ) {
            if ( string.IsNullOrEmpty( hangfireConnection ) )
                throw new Exception( "Connection string is not valid" );

            var options = new SqlServerStorageOptions {
                TransactionTimeout = TimeSpan.FromMinutes( 15 ),
                JobExpirationCheckInterval = TimeSpan.FromMinutes( 15 )
            };

            return new SqlServerStorage( hangfireConnection, options );
        }
    }
}